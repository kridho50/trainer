package com.example.latihanretrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("top-headlines/sources")
    Call<SourceResponse> getResource(@Query("apiKey") String apiKey);
}

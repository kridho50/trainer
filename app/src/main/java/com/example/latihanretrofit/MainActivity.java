package com.example.latihanretrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Api api;
    RecyclerView rvNews;
    NewsAdapter newsAdapter;
    List<SourceResponse.Source> sources = new ArrayList<>();
    Button btnSend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvNews = findViewById(R.id.rv_news);
        btnSend = findViewById(R.id.btn_send);
        getNews();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, IntentListDataActivity.class);
                Gson gson = new Gson();
                intent.putExtra("data", gson.toJson(sources));
                startActivity(intent);
            }
        });
    }

    private void getNews(){
        String apiKey = "5a78b282a4034cdaadfb2c564e6bd49e";

        api = ApiClient.getClient().create(Api.class);
        Call<SourceResponse> call = api.getResource(apiKey);
        call.enqueue(new Callback<SourceResponse>() {
            @Override
            public void onResponse(Call<SourceResponse> call, Response<SourceResponse> response) {
                if(response.isSuccessful()){
                    sources = response.body().getSources();
                    setDataAdapterNews(sources);
                }
            }

            @Override
            public void onFailure(Call<SourceResponse> call, Throwable t) {

            }
        });
    }

    private void setDataAdapterNews(List<SourceResponse.Source> sources) {
        newsAdapter = new NewsAdapter(MainActivity.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvNews.setLayoutManager(layoutManager);
        rvNews.setAdapter(newsAdapter);
        newsAdapter.setData(sources);
        newsAdapter.setOnClickLisetener(new NewsAdapter.OnClickItem() {
            @Override
            public void sendData(String title, String desc) {
                Intent intent = new Intent(MainActivity.this, IntentListDataActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("desc", desc);
                startActivity(intent);
            }
        });
    }
}
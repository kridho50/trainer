package com.example.latihanretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class IntentListDataActivity extends AppCompatActivity {
    TextView tvTitle, tvDesc;
    String title, description;
    String data;
    List<SourceResponse.Source> sources = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_list_data);

        title = getIntent().getStringExtra("title");
        description = getIntent().getStringExtra("desc");
        data = getIntent().getStringExtra("data");
        Type listType = new TypeToken<ArrayList<SourceResponse.Source>>(){}.getType();
        sources = new Gson().fromJson(data, listType);

        tvTitle = findViewById(R.id.tv_tittle);
        tvDesc = findViewById(R.id.tv_desc);

        tvTitle.setText(title);
        tvDesc.setText(description);
    }
}
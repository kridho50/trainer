package com.example.latihanretrofit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    Context context;
    private List<SourceResponse.Source>  sourceList = new ArrayList<>();

    public interface OnClickItem{
        void sendData(String title, String desc);

    }

    private OnClickItem onClickItem;


    public void setOnClickLisetener(OnClickItem onClickLisetener){
        this.onClickItem = onClickLisetener;
    }

    public NewsAdapter(Context context){
        this.context = context;
    }

    public void setData(List<SourceResponse.Source> sourceList){
        this.sourceList = sourceList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_view_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTittle.setText(sourceList.get(position).getName());
        holder.tvDesc.setText(sourceList.get(position).getDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickItem.sendData(sourceList.get(holder.getAdapterPosition()).getName(),
                        sourceList.get(holder.getAdapterPosition()).getDescription());
            }
        });
    }

    @Override
    public int getItemCount() {
        return sourceList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTittle, tvDesc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTittle = itemView.findViewById(R.id.tv_tittle);
            tvDesc = itemView.findViewById(R.id.tv_desc);
        }

    }
}
